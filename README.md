
# Powiększony nagłówek

Nagłówek 2
=======================

*Pierwszy* paragraf

**Drugi** paragraf

***Trzeci*** paragraf

~~Czwarty paragraf~~

>Piąty paragraf

>>>>Szósty paragraf


Lista 123
1. jeden
2. dwa
3. trzy
    1. trzy1
    2. trzy2
    3. trzy3
4. cztery

Lista abc
- a
- b
- c
    - c1
        - c2


```py
print("Witaj użytkowniku")
for i in range(3):
    print(i)
print("o_O")
```

Jakiś kod ``print("Hello ^^")``

![sadge_cat.jpeg](sadge_cat.jpeg)
